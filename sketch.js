// WORLD CONFIG
var GRAVITY = 0.001;
var LIFESPAN = 300;
var CYCLES = 0;
// ROCKET CONFIG
var NUM_ROCKETS = 10;
var MAX_VEL = 3;
var MAX_FORCE = 1;

// DNA CONFIG
var DNA_LEN = 100;
var CONTROL_CNT = 0;

var pMaxFit;
var rockets = [];
var target;
var grav;
function setup() {
  createCanvas(1200, 600);
  grav = createVector(0, GRAVITY);
  populate();
  target = new Target();
  pMaxFit = createP("");
}


function draw() {
  background(52);
  target.show();
  if (CYCLES < LIFESPAN) {
    for (var i = 0; i < NUM_ROCKETS; i++) {
      if (CONTROL_CNT < DNA_LEN) {
        if (frameCount % 10 == 0) {
          rockets[i].useControls();
          CONTROL_CNT++;
        }
      }
      rockets[i].gravity();
      rockets[i].update();
      rockets[i].show();
    }
    CYCLES++;
  } else {
    nextGenaration();
    CONTROL_CNT = 0;
    CYCLES = 0;
  }
}

function nextGenaration() {
  var topRocketId = 0;
  var maxFit = 0;
  for (var i = 0; i < NUM_ROCKETS; i++) {
    if (maxFit < rockets[i].fitness) {
      topRocketId = i;
      maxFit = rockets[i].fitness;
    }
  }
  pMaxFit.html(maxFit);
  for (var i = 0; i < NUM_ROCKETS; i++) {
    rockets[i].fitness /= maxFit;
  }

  var pool = [];
  for (var i = 0; i < NUM_ROCKETS; i++) {
    var n = rockets[i].fitness * 100;
    for (var k = 0; k < n; k++) {
      pool.push(rockets[i]);
    }
  }
  var winnerDna = rockets[topRocketId].dna;
  console.log(winnerDna);
  selection(pool, winnerDna);
}

function selection(pool, winner) {
  var winR = new Rocket(winner);
  console.log(winR);
  var nRockets = [];
  nRockets[0] = winR;

  for (var i = 1; i < NUM_ROCKETS; i++) {
    var parentA = random(pool).dna;
    var parentB = random(pool).dna;
    var child = parentA.crossover(parentB);
    child.mutation();
    var nR = new Rocket(child);
    nRockets[i] = nR;
  }


  rockets = nRockets;
}

function populate() {
  rockets = [];
  CONTROL_CNT = 0;
  for (var i = 0; i < NUM_ROCKETS; i++) {
    var dna = new Dna();
    rockets.push(new Rocket(dna));
  }
}
