function Dna(genTrans) {
  if (genTrans == undefined) {
    this.genes = [];
    for (var i = 0; i < DNA_LEN; i++) {
      var x = random(-MAX_FORCE, MAX_FORCE);
      var y = random(-MAX_FORCE, MAX_FORCE);
      this.genes.push(createVector(x, y));
    }
  } else {
    this.genes = genTrans;
  }

  this.crossover = function(other) {
    var newgenes = [];
    var mid = floor(random(this.genes.length));
    for (var i = 0; i < this.genes.length; i++) {
      if (i > mid) {
        newgenes[i] = this.genes[i];
      } else {
        newgenes[i] = other.genes[i];
      }
    }
    return new Dna(newgenes);
  }

  this.mutation = function() {
    for (var i = 0; i < this.genes.length; i++) {
      if (random(1) < 0.01) {
        this.genes[i] = p5.Vector.random2D();
        this.genes[i].setMag(MAX_FORCE);
      }
    }
  }
}
