function Rocket(dna) {
  this.dna = dna;
  this.pos = createVector(width / 2, height - 20);
  this.vel = createVector(0, -1);
  this.acc = createVector(0, -0.01);
  this.angle = 0;
  this.crashed = 0;
  this.fitness = 0;

  this.useControls = function() {
    this.burn(this.dna.genes[CONTROL_CNT]);
  }

  this.burn = function(thrust) {
    if (this.crashed == 0) {
      var nangle = PI - this.angle;
      thrust = thrust.rotate(nangle);
      //console.log(thrust.x + " | " + thrust.y);
      this.acc = this.acc.add(thrust).mult(0.5);
    //console.log(this.acc.x + " | " + this.acc.y);
    }
  }

  this.calcFitness = function() {
    var dis = dist(this.pos.x, this.pos.y, target.pos.x, target.pos.y);
    var fit = map(dis, 0, width, 100, 0);
    this.fitness = fit;

  }

  this.update = function() {
    if (this.crashed == 0) {
      this.vel = this.vel.add(this.acc);
      if (this.vel.mag() >= MAX_VEL) {
        this.vel.setMag(MAX_VEL);
      }
      this.pos = this.pos.add(this.vel);
      this.calcFitness();

      if (this.pos.y < 0) {
        this.crashed = 1;
        this.fitness *= .8;
      } else if (this.pos.x < 0 || this.pos.y < 0 || this.pos.x > width || this.pos.y > height) {
        this.crashed = 1;
        this.fitness *= .5;
      }
      if (dist(this.pos.x, this.pos.y, target.pos.x, target.pos.y) < 25) {
        this.crashed = 2;
        this.fitness *= 10;
      }
    }
  }

  this.gravity = function() {
    this.acc.add(grav);
  }

  this.show = function() {
    this.angle = atan2(this.vel.x, this.vel.y);
    push();
    translate(this.pos.x, this.pos.y)
    color(255);
    rotate(PI - this.angle);
    noStroke();
    if (!this.crashed) {
      fill(255);
    } else if (this.crashed == 1) {
      fill(255, 0, 0);
    } else {
      fill(0, 255, 0);
    }
    ellipse(0, 0, 10, 25);
    fill(40);
    ellipse(0, 0 - 5, 4);
    fill(255);
    triangle(0, 0 + 12, 0 + 5, 0 + 20, 0 - 5, 0 + 20);
    fill(0, 255, 255);
    textSize(10);
    text(this.fitness, 10, 20);
    pop();
  };
}
