function Target() {
  this.pos = createVector(width / 2, 0 + 50);
  this.show = function() {
    push();
    translate(this.pos.x, this.pos.y);
    noStroke();
    fill(200);
    ellipse(0, 0, 50);
    pop();
  }
}
